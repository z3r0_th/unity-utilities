﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadImageFromRemote : MonoBehaviour {

    public MeshRenderer[] loadImagesInto;
    public Image[] images;

    public string url;
    public string imageUrl;

    void Start () {

        // Testing if we can handle the cache and do not load multiple instances from same url
        // You may use profile to check.
        foreach (MeshRenderer r in loadImagesInto)
        {
            MeshRenderer render = r;
            ImageLoader.LoadTextureFromUrl(url, delegate (Texture2D tex)
            {
                render.material.mainTexture = tex;
            });
        }


        // It will create new sprite for each image
        //but only one texture will be created
        //we may change to cache sprites too in the near future
        foreach(Image image in images)
        {
            Image img = image;
            ImageLoader.LoadSpriteFromUrl(imageUrl, delegate (Sprite s)
            {
                img.sprite = s;
            });
        }
	}
}
