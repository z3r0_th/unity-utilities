﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// At this moment to use "ClassSelectionAttribute" we have to use "MonoBehaviour". Thats why we implemented 'ClassBase'
/// </summary>
public class SelectClass : MonoBehaviour {

    [ClassSelection]
    public ClassBase selectInterface1;

}
