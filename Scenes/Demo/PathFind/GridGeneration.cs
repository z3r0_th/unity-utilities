﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridGeneration : MonoBehaviour {

    public AStar<GridCell> astar;
    public GridCell gridCell;
    public int width;
    public int height;
    public float space = 1;

    private GridCell [,] cells;
    private GridCell selected;

    public void GenerateGrid()
    {
        cells = new GridCell[width,height];
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                GridCell cell = GameObject.Instantiate(gridCell);
                cell.transform.position = new Vector3(space * i, 0, space * j);
                cells[i,j] = cell;
            }
        }

        for (int k = 0; k < width; ++k)
        {
            for (int m = 0; m < height; ++m)
            {
                GridCell cell = cells[k, m];

                if (m > 0)
                {
                    cell.left = cells[k, m - 1];
                    if (k > 0)
                        cell.downLeft = cells[k - 1, m - 1];
                    if (k < width - 1)
                        cell.upLeft = cells[k + 1, m - 1];
                }
                    
                if (m < height - 1)
                {
                    cell.right = cells[k, m + 1];
                    if (k > 0)
                        cell.downRight = cells[k - 1, m + 1];
                    if (k < width - 1)
                        cell.upRight = cells[k + 1, m + 1];
                }
                    
                if (k > 0)
                    cell.down = cells[k - 1, m];
                if (k < width-1)
                    cell.up = cells[k + 1, m];
            }            
        }
    }

	void Start () {
        GenerateGrid();

        astar = new AStar<GridCell>();
        astar.DifficultToWalkDelegate = delegate (GridCell from, GridCell to) { return 1; };
        astar.GetNeighboorDelegate = delegate (GridCell from)
        {
            List<GridCell> neighbors = new List<GridCell>();
            if (from.left != null)
                neighbors.Add(from.left);

            if (from.downLeft != null)
                neighbors.Add(from.downLeft);

            if (from.upLeft != null)
                neighbors.Add(from.upLeft);

            if (from.right != null)
                neighbors.Add(from.right);

            if (from.downRight != null)
                neighbors.Add(from.downRight);

            if (from.upRight != null)
                neighbors.Add(from.upRight);

            if (from.up != null)
                neighbors.Add(from.up);

            if (from.down != null)
                neighbors.Add(from.down);

            return neighbors.ToArray();
        };
        astar.HeuristicDistanceDelegate = delegate (GridCell from, GridCell to) 
        {
            return Vector3.Distance(from.transform.position, to.transform.position);
        };
        astar.IsWalkableDelegate = delegate (GridCell cell)
        {
            return cell.walkable;
        };
    }
	
	void Update () {

        for (int i = 0; i < width; ++i)
        {
            for (int j = 0; j < width; ++j)
            {
                GridCell cell = cells[i, j];

                if (cell.walkable)
                    cell.GetComponent<MeshRenderer>().material.color = Color.white;
                else
                    cell.GetComponent<MeshRenderer>().material.color = Color.red;

                if (cell.path)
                    cell.GetComponent<MeshRenderer>().material.color = Color.yellow;
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, float.MaxValue))
            {
                if (selected != null)
                {
                    for (int i = 0; i < width; ++i)
                    {
                        for (int j = 0; j < width; ++j)
                        {
                            cells[i, j].path = false;
                        }
                    }

                    GridCell[] path;
                    if (astar.FindPath(hit.transform.GetComponent<GridCell>(), selected, out path))
                    {
                        foreach (GridCell c in path) c.path = true;
                    }
                    selected = null;
                    return;
                }

                selected = hit.transform.GetComponent<GridCell>();
                selected.path = true;
                if (!selected.walkable)
                {
                    selected.path = false;
                    selected = null;
                }
            }
        }
        
    }
}
