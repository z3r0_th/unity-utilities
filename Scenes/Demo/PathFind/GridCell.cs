﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridCell : MonoBehaviour {
    public GridCell right;
    public GridCell left;
    public GridCell up;
    public GridCell down;
    public GridCell upLeft;
    public GridCell upRight;
    public GridCell downLeft;
    public GridCell downRight;

    public bool walkable = true;
    public bool path;
}
