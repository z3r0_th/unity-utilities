﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeTriggerDemo : MonoBehaviour {

	// Use this for initialization
	void Start () {
        TimeTrigger.SetTrigger(5, delegate ()
        {
            Debug.Log("Time Finished");
        });

        TimeTrigger.SetTrigger(1, delegate (TimeTrigger t)
        {
            Debug.Log("Time Update: " + t.Time);
        }, delegate ()
        {
            Debug.Log("Time Finished =)");
        });
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
