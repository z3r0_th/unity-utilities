﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System;

public static class ReflectionUtil
{

    public static System.Type[] GetTypesInheritanceFrom(System.Type baseType, bool canBeAbstract = false, bool canBeInterface = false)
    {
        return System.AppDomain.CurrentDomain.GetAssemblies().SelectMany(s => 
        s.GetTypes()).Where(p => baseType.IsAssignableFrom(p) && (canBeInterface || p.IsClass) && (canBeAbstract || !p.IsAbstract)).ToArray();
    }

    /// <summary>
    /// Call a method using reflection.
    /// Object to call the method from, the method name and it's parameters
    /// return whatever the method should return, or return null if there was a problem
    /// use last out argument 'couldCall' to check if method was indeed called
    /// </summary>
    public static object CallMethod(object obj, string methodName, object[] parameters, out bool couldCall)
    {
        couldCall = false;
        MethodInfo method = obj.GetType().GetMethod(methodName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
        if (method == null) return null;
        try
        {
            couldCall = true;
            return method.Invoke(obj, parameters);
        }
        catch (System.Exception e)
        {
            couldCall = false;
            Debug.LogWarning("Could not call method " + methodName + " on object type " + obj.GetType().Name + " because: " + e.ToString());
            return null;
        }
    }

    public static object CallMethod(object obj, string methodName, object[] parameters = null)
    {
        bool whatever = false;
        return CallMethod(obj, methodName, parameters, out whatever);
    }


    /// <summary>
    /// Check if this obj is an array or list type. the is assigned to ref listType
    /// if neither, return false, if one of them, return true
    /// </summary>
    public static bool IsArrayOrList(System.Type obj, ref System.Type listType)
    {
        foreach (System.Type interfaceType in obj.GetInterfaces())
        {
            if (interfaceType.IsGenericType &&
                interfaceType.GetGenericTypeDefinition() == typeof(IList<>))
            {

                if (obj.GetGenericArguments() != null && obj.GetGenericArguments().Length != 0)
                {
                    listType = obj.GetGenericArguments()[0];
                }
                else if (obj.GetElementType() != null)
                {
                    listType = obj.GetElementType();
                }

                return true;
            }
        }

        return false;
    }

}