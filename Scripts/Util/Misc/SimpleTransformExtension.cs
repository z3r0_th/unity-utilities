﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SimpleTransformObjectExtension {
    public static T GetComponentInParents<T>(this Transform transform)
    {
        if (transform == null) return default(T);
        T c = transform.GetComponent<T>();
        if (c != null) return c;

        return GetComponentInParents<T>(transform.parent);
    }

    public static void SetWholeObjectLayer(this Transform transform, int layer)
    {
        transform.gameObject.layer = layer;
        foreach(Transform t in transform)
        {
            t.SetWholeObjectLayer(layer);
        }
    }
}
