﻿using UnityEngine;
using System.Collections;

public class DestroyMeAfter : MonoBehaviour {

    public float delay = 1;
    public bool onStart = true;

	void Start () {
	    if (onStart)
        {
            StartCoroutine(Timer(delay));
        }
	}

    IEnumerator Timer(float delay)
    {
        yield return new WaitForSeconds(delay);
        if (Application.isPlaying)
            Destroy(gameObject);
        else
            DestroyImmediate(gameObject);
    }
    
    public void DestroyAfter(float delay)
    {
        StartCoroutine(Timer(delay));
    }

    public void DestroyAfter()
    {
        DestroyAfter(delay);
    }
}
