﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Fader))]
public class FaderInspector : Editor
{
    public override void OnInspectorGUI()
    {
        Fader myTarget = (Fader)target;
        
        var destroyOnCompleteField = serializedObject.FindProperty("destroyOnComplete");
        var fadeOnStartField = serializedObject.FindProperty("fadeOnStart");
        var groupFadeTargetField = serializedObject.FindProperty("groupFadeTarget");
        var graphicFadeTargetField = serializedObject.FindProperty("graphicFadeTarget");
        var timeField = serializedObject.FindProperty("time");
        var curveField = serializedObject.FindProperty("curve");
        var fadeCompletedField = serializedObject.FindProperty("fadeCompleted");

        EditorGUILayout.PropertyField(destroyOnCompleteField);
        EditorGUILayout.PropertyField(fadeOnStartField);

        if (myTarget.GetComponent<CanvasGroup>() != null)
            EditorGUILayout.PropertyField(groupFadeTargetField);
        else if (myTarget.GetComponent<UnityEngine.UI.Graphic>() != null || myTarget.GetComponent<Renderer>() != null)
            EditorGUILayout.PropertyField(graphicFadeTargetField);

        EditorGUILayout.PropertyField(timeField);
        EditorGUILayout.PropertyField(curveField);
        EditorGUILayout.PropertyField(fadeCompletedField);

        serializedObject.ApplyModifiedProperties();
    }
}
