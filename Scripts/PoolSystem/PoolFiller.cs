﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolFiller : MonoBehaviour {

    public int Step = 100;
    public void FillPool<T>(Pool<T> pool, int count, System.Action<float> progress) where T : Component
    {
        StartCoroutine(Fill<T>(pool, count, progress));
    }

    IEnumerator Fill<T>(Pool<T> pool, int count, System.Action<float> progress) where T : Component
    {
        int step = Step;
        int i = 0;

        for (; i < count; i+=step)
        {
            pool.Warm(step);
            progress(i/(float)count);
            yield return null;
        }

        for (; i < count; i ++)
        {
            pool.Warm(1);
            progress(i / (float)count);
            yield return null;
        }

        progress(1);
        Destroy(gameObject);
    }
}
