﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolSystem {

    private static Hashtable poolTable = new Hashtable();

    public static Pool<T> Pool<T>(T prefab = null, bool instantiateObjectWhenEmpty = true, bool prewarm = true, int preWarmCount = 10) where T : Component
    {
        string hash = typeof(T).FullName;
        Pool<T> pool = (Pool<T>)poolTable[hash];
        if (pool == null)
        {
            Debug.LogWarning("Pool Requested do not exist. Attempt to create new one");
            pool = new global::Pool<T>(prefab, instantiateObjectWhenEmpty, prewarm, preWarmCount);
            poolTable.Add(hash, pool);
        }

        return pool;
    }
}
