﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[ExecuteInEditMode]
public class GitInfoPanel : MonoBehaviour {

    private static bool preservePanel = false;
    public static bool ShowInfo = true;

    public bool resetLayout = true;
    public Canvas canvas;
	public RectTransform infoPanel;
	public Text branchText;
	public Text hashText;
	public Text tagText;

    private RectTransform background;

	void Start () {
		CreateInfoPanel ();
        if (infoPanel != null)
        {
            infoPanel.gameObject.SetActive(ShowInfo);
        }
	}

	void OnValidate() {
		CreateInfoPanel ();
	}

	void OnEnable() {
		CreateInfoPanel ();
	}

    void OnDestroy()
    {
        if (!preservePanel)
            if (Application.isPlaying)
                Destroy(infoPanel.gameObject);
            else 
                DestroyImmediate(infoPanel.gameObject);
    }

    Canvas GetCanvasOnParent(Transform t)
    {
        Canvas c = t.GetComponentInParent<Canvas>();
        if (c != null) return c;
        foreach(Transform child in t)
        {
            c = GetCanvasOnParent(child);
            if (c != null)
            {
                return c;
            }
        }

        return null;
    }

    private Canvas CreateOverlayCanvas(GameObject overlayCanvasGO)
    {
        overlayCanvasGO.transform.position = Vector3.zero;
        Canvas overlayCanvas = overlayCanvasGO.AddComponent<Canvas>();
        overlayCanvas.renderMode = RenderMode.ScreenSpaceOverlay;
        overlayCanvas.worldCamera = Camera.main;
        overlayCanvasGO.AddComponent<CanvasScaler>();
        overlayCanvasGO.AddComponent<GraphicRaycaster>();

        return overlayCanvas;
    }

    void CreateInfoPanel() {
		if (canvas == null) {
            canvas = GetCanvasOnParent(transform);
            if (canvas == null) {
                canvas = CreateOverlayCanvas(gameObject);
			}
		}

        if (infoPanel == null) {
			GameObject panel = new GameObject ("Git info panel");
			panel.transform.SetParent (canvas.transform, true);
			infoPanel = panel.AddComponent<RectTransform> ();
			CanvasGroup canvasGroup = panel.AddComponent<CanvasGroup> ();
			canvasGroup.blocksRaycasts = false;
		}

        if (background == null)
        {
            GameObject panel = new GameObject("background");
            panel.transform.SetParent(canvas.transform, true);
            background = (panel.AddComponent<Image>()).GetComponent<RectTransform>();
            background.GetComponent<Image>().color = new Color(0.0f, 0.0f, 0.0f, 0.25f);
        }

        //Create labels


        //Hash
        if (hashText == null) {
			GameObject hashGo = new GameObject ("HashText");
			hashText = hashGo.AddComponent<Text> ();
		}

        //Branch

        if (branchText == null)
        {
            GameObject branchGo = new GameObject("BranchText");
            branchText = branchGo.AddComponent<Text>();
        }

        //tag

        if (tagText == null)
        {
            GameObject tagGo = new GameObject("TagText");
            tagText = tagGo.AddComponent<Text>();
        }

        hashText.text = GitInfo.GetCurrentCommitHash();
        branchText.text = GitInfo.GetCurrentBranch();
        tagText.text = GitInfo.GetLastTag();

        if (resetLayout)
        {
            infoPanel.SetParent(canvas.transform, true);
            infoPanel.SetAsLastSibling();
            infoPanel.anchorMax = new Vector2(1, 0);
            infoPanel.anchorMin = new Vector2(0, 0);
            infoPanel.offsetMax = new Vector2(-2, 0);
            infoPanel.offsetMin = new Vector2(2, 2);

            background.transform.SetParent(infoPanel.transform, true);
            background.pivot = new Vector2(0.5f, 0);
            background.anchorMax = new Vector2(1, 0);
            background.anchorMin = new Vector2(0, 0);
            background.offsetMax = Vector2.zero;
            background.offsetMin = Vector2.zero;
            background.sizeDelta = new Vector2(0, 40);

            hashText.transform.SetParent(infoPanel.transform, true);
            RectTransform hashRect = hashText.GetComponent<RectTransform>();

            hashRect.pivot = new Vector2(0.5f, 0);
            hashRect.anchorMax = new Vector2(1, 0);
            hashRect.anchorMin = new Vector2(0, 0);
            hashRect.offsetMax = new Vector2(-2, 0); // Vector2.zero;
            hashRect.offsetMin = Vector2.zero;
            hashRect.sizeDelta = new Vector2(0, 18);

            hashText.alignment = TextAnchor.LowerRight;

            branchText.transform.SetParent(infoPanel.transform, true);
            RectTransform branchRect = branchText.GetComponent<RectTransform>();

            branchRect.pivot = new Vector2(0.5f, 0);
            branchRect.anchorMax = new Vector2(1, 0);
            branchRect.anchorMin = new Vector2(0, 0);
            branchRect.offsetMax = new Vector2(-2, 18 * 2);//new Vector2(0, 18*2);
            branchRect.offsetMin = new Vector2(0, 18);
            branchRect.sizeDelta = new Vector2(0, 18);

            branchText.alignment = TextAnchor.LowerRight;

            tagText.transform.SetParent(infoPanel.transform, true);
            RectTransform tagRect = tagText.GetComponent<RectTransform>();

            tagRect.pivot = new Vector2(0.5f, 0);
            tagRect.anchorMax = new Vector2(1, 0);
            tagRect.anchorMin = new Vector2(0, 0);
            tagRect.offsetMax = new Vector2(-2, 18 * 4);// new Vector2(0, 18*4);
            tagRect.offsetMin = new Vector2(0, 18 * 2);
            tagRect.sizeDelta = new Vector2(0, 18);

            tagText.alignment = TextAnchor.LowerRight;

            if (tagText.text != "")
            {
                background.sizeDelta = new Vector2(0, 54);
            }

            resetLayout = false;
        }
        
    }
}
