﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class GitInfoGUI : MonoBehaviour {

    public float areaHeight = 45;
    public float space = -12;

    private void OnGUI()
    {
        GUIStyle centeredStyle = GUI.skin.GetStyle("Label");
        centeredStyle.alignment = TextAnchor.MiddleRight;

        GUILayout.BeginArea(new Rect(10, Screen.height - areaHeight, Screen.width - 10, areaHeight));
        GUILayout.BeginVertical();

        if (GitInfo.GetLastTag() != "")
        {
            GUILayout.Label("Tag: " + GitInfo.GetLastTag(), centeredStyle);
        } else
        {
            GUILayout.Label("");
        }
        GUILayout.Space(space);
        GUILayout.Label("Branch: " + GitInfo.GetCurrentBranch(), centeredStyle);
        GUILayout.Space(space);
        GUILayout.Label("Hash: " + GitInfo.GetCurrentCommitHash(), centeredStyle);
        
        GUILayout.EndVertical();
        GUILayout.EndArea();
    }
}
