﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorSequence : MonoBehaviour {

    [System.Serializable]
    public class AnimatorEvent : UnityEngine.Events.UnityEvent { }


    [SerializeField]
    private Animator animator;

    [System.Serializable]
    private struct AnimatorTrigger
    {
        public Animator anim;
        public AnimatorEvent evnt;
        public string trigger;
        public float delay;
    }


    [System.Serializable]
    private struct AnimatorTriggerSequence
    {
        public AnimatorTrigger []  triggers;
    }


    [SerializeField]
    private AnimatorTriggerSequence absoluteSequence;

    [SerializeField]
    private AnimatorTriggerSequence sequencialSequence;

    [SerializeField]
    private bool InitOnStart = false;

    private float totalTime = -1;

    public float TotalTime
    {
        get
        {
            if (totalTime > 0) return totalTime;
            float biggestTime = -1;
            foreach(AnimatorTrigger c in absoluteSequence.triggers)
            {
                if (c.delay > biggestTime) biggestTime = c.delay;
            }
            float totalSequence = 0;
            foreach(AnimatorTrigger c in sequencialSequence.triggers)
            {
                totalSequence += c.delay;
            }
            totalTime = ((totalSequence > biggestTime) ? totalSequence : biggestTime);
            return totalTime;
        }
    }


    public void Play()
    {
        foreach (AnimatorTrigger seq in absoluteSequence.triggers)
        {
            StartCoroutine(WaitAndPlay(seq));
        }

        if (sequencialSequence.triggers.Length > 0)
            StartCoroutine(WaitAndPlaySequence(sequencialSequence.triggers[0], sequencialSequence, 1));
    }

    private void OnValidate()
    {
        if (animator == null) animator = gameObject.FindInDeepChildren<Animator>();
    }

    private void Start () {
        if (animator == null) animator = gameObject.FindInDeepChildren<Animator>();
		if (InitOnStart)
        {
            Play();
        }
	}

    private IEnumerator WaitAndPlay(AnimatorTrigger seq)
    {
        yield return new WaitForSeconds(seq.delay);
        Play(seq);
    }

    private IEnumerator WaitAndPlaySequence(AnimatorTrigger seq, AnimatorTriggerSequence list, int index)
    {
        yield return new WaitForSeconds(seq.delay);
        Play(seq);

        if (index < list.triggers.Length)
            StartCoroutine(WaitAndPlaySequence(list.triggers[index], list, index + 1));
    }


    private void Play(AnimatorTrigger seq)
    {
        Animator anim = seq.anim;
        if (anim == null)
            anim = animator;

        if (anim != null)
            anim.SetTrigger(seq.trigger);

        if (seq.evnt != null)
            seq.evnt.Invoke();
    }
}
