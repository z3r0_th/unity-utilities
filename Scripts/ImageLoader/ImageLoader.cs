﻿using UnityEngine;
using UnityEngine.Assertions;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.IO;


public static class ImageLoader {

    #region CacheManagement

    private static bool caching = true;
    private static CachingImages Cache
    {
        get
        {
            if (cache == null)
            {
                cache = new CachingImages();
                cache.caching = caching;
            }

            return cache;
        }
    }
    private static CachingImages cache;


    public static void Unload(string path)
    {
        Cache.Unload(path);
    }

    public static void UnloadAll()
    {
        Cache.UnloadAll();
    }

    #endregion

    #region RemoteImageLoader 

    /// <summary>
    /// Loads texture from specific url and caches it. 
    /// Or loads from cache if avaliable.
    /// </summary>
    public static void LoadTextureFromUrl(string _url, System.Action<Texture2D> _callback)
    {
        Assert.IsNotNull(_callback);
        string url = _url;
        System.Action<Texture2D> callback = _callback;

        Texture2D tex = Cache.GetTexture(url);
        if (tex != null)
        {
            callback(tex);
            return;
        }

        //If I am the first one to show the intent to load this image, 
        //than register the callback and start the loading the texture from remote (Register the callback if im not the first one anyway)
        //When it is finished, put it on cache, that will trigger ALL the callbacks registered before for this image
        if (Cache.RegisterCacheIntent(url, delegate (Texture2D texture)
        {
            callback(texture);
        }))
        {
            RemoteImageLoader.LoadTextureFromUrl(url, delegate (Texture2D texture)
            {
                Cache.Cache(url, texture);
            });
        }
    }

    /// <summary>
    /// Loads a texture (from cache if possible) and creates the sprite
    /// </summary>
    public static void LoadSpriteFromUrl(string _url, Rect _rect, Vector2 _pivot, System.Action<Sprite> _callback)
    {
        Assert.IsNotNull(_callback);
        string url = _url;
        Rect rect = _rect;
        Vector2 pivot = _pivot;
        System.Action<Sprite> callback = _callback;

        LoadTextureFromUrl(url, delegate (Texture2D tex)
        {
            if (tex == null)
            {
                callback(null);
                return;
            }

            callback(Sprite.Create(tex, rect, pivot));
        });
    }

    /// <summary>
    /// Loads a texture (from cache if possible) and creates the sprite
    /// </summary>
    public static void LoadSpriteFromUrl(string _url, System.Action<Sprite> _callback)
    {
        Assert.IsNotNull(_callback);
        string url = _url;
        System.Action<Sprite> callback = _callback;

        LoadTextureFromUrl(url, delegate (Texture2D tex)
        {
            if (tex == null)
            {
                callback(null);
                return;
            }

            Vector2 pivot = new Vector2(0.5f, 0.5f);
            Rect rect = new Rect(0, 0, tex.width, tex.height);
            callback(Sprite.Create(tex, rect, pivot));
        });
    }

    #endregion

    #region LocalImageLoader

    public static Texture2D LoadTextureFromFile(string path)
    {
        Texture2D tex = Cache.GetTexture(path);
        if (tex == null)
        {
            tex = LocalImageLoader.LoadTextureFromFile(path);
            Cache.Cache(path, tex);
            if (tex == null) return null;
        }

        return tex;
    }

    public static Sprite LoadSpriteFromFile(string path, Rect rect, Vector2 pivot)
    {
        Texture2D tex = LoadTextureFromFile(path);
        if (tex == null) return null;
        return Sprite.Create(tex, rect, pivot);
    }

    public static Sprite LoadSpriteFromFile(string path)
    {
        Texture2D tex = LoadTextureFromFile(path);
        if (tex == null) return null;
        return Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
    }

    public static Texture2D[] LoadAllTexturesFromFolder(string folderPath, bool recursive = false)
    {
        string[] textureFiles = LocalImageLoader.GetAllImageFilesFromFolder(folderPath, recursive);
        List<Texture2D> textures = new List<Texture2D>();
        foreach (string file in textureFiles)
        {
            textures.Add(LoadTextureFromFile(file));
        }

        return textures.ToArray();
    }

    public static Sprite[] LoadAllSpritesFromFolder(string folderPath, bool recursive = false)
    {
        string[] spriteFiles = LocalImageLoader.GetAllImageFilesFromFolder(folderPath, recursive);
        List<Sprite> sprites = new List<Sprite>();
        foreach (string file in spriteFiles)
        {
            sprites.Add(LoadSpriteFromFile(file));
        }

        return sprites.ToArray();
    }

    private static void AddTexture(Texture2D texture, List<Texture2D> textures, int count, System.Action<Texture2D[]> callback, System.Action<float> progressCallback)
    {
        textures.Add(texture);

        if (textures.Count == count)
        {
            callback(textures.ToArray());
        }
        if (progressCallback != null)
        {
            progressCallback((float)textures.Count / count);
        }
    }

    public static void LoadAllTexturesFromFolderAsync(string folderPath, bool recursive, System.Action<Texture2D[]> _callback, System.Action<float> progressCallback = null)
    {
        Assert.IsNotNull(_callback);
        System.Action<Texture2D[]> callback = _callback;
        string[] textureFiles = LocalImageLoader.GetAllImageFilesFromFolder(folderPath, recursive);
        List<Texture2D> textures = new List<Texture2D>();
        if (textureFiles == null || textureFiles.Length == 0) callback(textures.ToArray());

        foreach (string _file in textureFiles)
        {
            string file = @"file:///" + Application.dataPath + "/../" + _file;
            file = file.Replace(@"\", @"/");
            Texture2D t = Cache.GetTexture(file);
            if (t != null)
            {
                AddTexture(t, textures, textureFiles.Length, callback, progressCallback);
                continue;
            }

            if (Cache.RegisterCacheIntent(file, delegate (Texture2D tex)
            {
                AddTexture(tex, textures, textureFiles.Length, callback, progressCallback);
            }))
            {
                LocalImageLoader.LoadTextureFromFileAsync(file, delegate (Texture2D tex)
                {
                    Cache.Cache(file, tex);
                });
            }            
        }
    }

    #endregion

    #region CachingClass

    /// <summary>
    /// Helper class. Used to cache some images and avoid double loading
    /// </summary>
    private class CachingImages
    {
        public bool caching = true;

        private int totalCacheSize = 10000000; // ~10 Mega bytes
        private int cacheSize = 0;

        private Dictionary<string, Texture2D> cache = new Dictionary<string, Texture2D>();
        private Dictionary<string, List<System.Action<Texture2D>>> textureCallback = new Dictionary<string, List<System.Action<Texture2D>>>();
        private List<string> lastUsedTextures = new List<string>();

        public float CacheUsed
        {
            get
            {
                return ((float)cacheSize) / totalCacheSize;
            }
        }

        /// <summary>
        /// Signal that the image in this path has started to be loaded. And we are interested in the image. If/when it completes, the callback is triggered.
        /// If intent is claimed for the first time, it will return true. False otherwise.
        /// If it returned true, client MUST implements a way to cache the image.
        /// </summary>
        public bool RegisterCacheIntent(string path, System.Action<Texture2D> callback)
        {
            if (cache.ContainsKey(path))
            {
                if (callback != null)
                    NotifyOn(path, callback);
                return false;
            }
            cache.Add(path, null);
            if (callback != null)
                NotifyOn(path, callback);
            return true;
        }

        /// <summary>
        /// Register a callback to be triggered when the image has downloaded
        /// </summary>
        public void NotifyOn(string path, System.Action<Texture2D> callback)
        {
            if (callback == null) return;
            if (!cache.ContainsKey(path)) return;
            List<System.Action<Texture2D>> callbackList;

            if (!textureCallback.TryGetValue(path, out callbackList))
            {
                callbackList = new List<System.Action<Texture2D>>();
                textureCallback.Add(path, callbackList);
            }

            callbackList.Add(callback);
        }       

        /// <summary>
        /// Cache the texture on this path
        /// </summary>
        public void Cache(string path, Texture2D tex)
        {
            cache.Remove(path);

            if (caching && tex != null)
            {
                cache.Add(path, tex);
                cacheSize += TextureSize(tex);
                Using(path);

                if (cacheSize > totalCacheSize)
                {
                    CleanCache();
                }
            }

            List<System.Action<Texture2D>> callbackList;

            if (textureCallback.TryGetValue(path, out callbackList))
            {
                foreach (System.Action<Texture2D> callback in callbackList)
                {
                    if (callback != null)
                        callback(tex);
                }
                callbackList.Clear();
                textureCallback.Remove(path);
            }
        }

        /// <summary>
        /// Is this texture/sprite been loading?
        /// </summary>
        public bool Loading(string path)
        {
            if (!cache.ContainsKey(path)) return false;
            return cache[path] == null;
        }


        /// <summary>
        /// Unload all images from this Cache
        /// </summary>
        public void UnloadAll()
        {
            cache.Clear();
            lastUsedTextures.Clear();
            cacheSize = 0;
            textureCallback.Clear();
        }

        /// <summary>
        /// Get the texture cached on path. Null if no texture cached.
        /// </summary>
        public Texture2D GetTexture(string path)
        {
            if (cache.ContainsKey(path))
            {
                Using(path);
                return cache[path];
            }

            return null;
        }

        /// <summary>
        /// Unload specific asset.
        /// </summary>
        public void Unload(string path)
        {
            Texture2D toRemove = null;
            if (cache.TryGetValue(path, out toRemove))
            {
                cacheSize -= TextureSize(toRemove);
            }

            cache.Remove(path);
            lastUsedTextures.Remove(path);            
            textureCallback.Remove(path);
        }

        private int TextureSize(Texture2D tex)
        {
            return tex.GetRawTextureData().Length;
        }

        private void Using(string path)
        {
            int index = lastUsedTextures.IndexOf(path);
            if (index < 0)
            {
                lastUsedTextures.Insert(0, path);
                return;
            }
            lastUsedTextures.RemoveAt(index);
            lastUsedTextures.Insert(0, path);
        }

        private void CleanCache()
        {
            int removing = lastUsedTextures.Count - 1;
            while (cacheSize > totalCacheSize && removing > 0)
            {
                Unload(lastUsedTextures[removing]);
                removing--;
            }

            if (cacheSize > totalCacheSize)
            {
                Debug.LogWarning("Could not clean cache properly: " + cacheSize);
            }
        }
    }

    #endregion

}
