﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class LocalImageLoader : MonoBehaviour {

    /// <summary>
    /// Loads Sprite from path. It will load texture and create ONE sprite with specified rect and pivot.
    /// Return null if path is invalid.
    /// </summary>
    public static Sprite LoadSpriteFromFile(string path, Rect rect, Vector2 pivot)
    {
        Texture2D tex = LoadTextureFromFile(path);
        if (tex == null) return null;

        return Sprite.Create(tex, rect, pivot);
    }

    /// <summary>
    /// Loads Sprite from path. It will load texture and create ONE sprite from entire texture with center pivot.
    /// Return null if path is invalid.
    /// </summary>
    public static Sprite LoadSpriteFromFile(string path)
    {
        Texture2D tex = LoadTextureFromFile(path);
        if (tex == null) return null;

        return Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
    }

    /// <summary>
    /// Loads texture from path.
    /// Return null if path is invalid.
    /// </summary>
    public static Texture2D LoadTextureFromFile(string path)
    {
        Texture2D tex = null;

        if (File.Exists(path))
        {
            byte[] fileData = File.ReadAllBytes(path);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData);
            tex.name = path;
        }

        return tex;
    }

    /// <summary>
    /// Loads all textures from specific folder, may be recursive.
    /// It blocks thread - See LoadAllTexturesFromFolderAsync to load async
    /// </summary>
    public static Texture2D[] LoadAllTexturesFromFolder(string folderPath, bool recursive = false)
    {
        string[] textureFiles = GetAllImageFilesFromFolder(folderPath, recursive);
        List<Texture2D> textures = new List<Texture2D>();
        foreach (string file in textureFiles)
        {
            textures.Add(LoadTextureFromFile(file));
        }

        return textures.ToArray();
    }

    /// <summary>
    /// Loads ONE sprites from each texture in the specific folder, may be recursive.
    /// It blocks thread - See LoadAllSpritesFromFolderAsync to load async
    /// </summary>
    public static Sprite[] LoadAllSpritesFromFolder(string folderPath, bool recursive = false)
    {
        string[] spriteFiles = GetAllImageFilesFromFolder(folderPath, recursive);
        List<Sprite> sprites = new List<Sprite>();
        foreach (string file in spriteFiles)
        {
            sprites.Add(LoadSpriteFromFile(file));
        }

        return sprites.ToArray();
    }

    /// <summary>
    /// Loads all textures from specific folder, may be recursive.
    /// It implements callback to show progress.
    /// </summary>
    public static void LoadAllTexturesFromFolderAsync(string folderPath, bool recursive, System.Action<Texture2D[]> callback, System.Action<float> progress = null)
    {
        string[] textureFiles = GetAllImageFilesFromFolder(folderPath, recursive);
        GameObject go = new GameObject("Load Textures Async");
        LocalImageLoader loader = go.AddComponent<LocalImageLoader>();
        loader.StartCoroutine(loader.LoadAllTexturesAsync(textureFiles, callback, progress, true));
    }

    public static void LoadTextureFromFileAsync(string file, System.Action<Texture2D> callback)
    {
        GameObject go = new GameObject("Load Texture Async");
        LocalImageLoader loader = go.AddComponent<LocalImageLoader>();
        loader.StartCoroutine(loader.LoadTextureAsync(file, callback));
    }


    /// <summary>
    /// Return a list of all image files (png or jpg/jpeg) from specific folder. May look recursivally with "recursive" prameter
    /// </summary>
    public static string[] GetAllImageFilesFromFolder(string folderPath, bool recursive)
    {
        List<string> list = new List<string>();

        if (Directory.Exists(folderPath))
        {
            list.AddRange(Directory.GetFiles(folderPath, "*.png"));
            list.AddRange(Directory.GetFiles(folderPath, "*.jpg"));
            list.AddRange(Directory.GetFiles(folderPath, "*.jpeg"));

            if (recursive)
            {
                string[] subFolders = Directory.GetDirectories(folderPath);
                foreach (string folder in subFolders)
                {
                    list.AddRange(GetAllImageFilesFromFolder(folder, recursive));
                }
            }
        }

        return list.ToArray();
    }


    /// <summary>
    /// Coroutine to load all textures "Async".
    /// </summary>
    private IEnumerator LoadAllTexturesAsync(string [] files, System.Action<Texture2D[]> callback, System.Action<float> progressCallback, bool destroyWhenFinish = false)
    {
        List<Texture2D> textures = new List<Texture2D>();
        int step = 2;
        for (int i = 0; i < files.Length; ++i)
        {
            if (i % step == 0)
            {
                if (progressCallback != null)
                    progressCallback((float)i / files.Length);
                yield return null;
            }
            string file = files[i];
            textures.Add(LoadTextureFromFile(file));
        }

        if (destroyWhenFinish) Destroy(gameObject);

        if (callback != null)
            callback(textures.ToArray());
    }

    private IEnumerator LoadTextureAsync(string file, System.Action<Texture2D> _callback)
    {
        string fileName = file;
        System.Action<Texture2D> callback = _callback;
        WWW www = new WWW(file);
        yield return www;

        if (www.error != null && www.error != "")
        {
            Debug.LogWarning("Could not open file " + file + " with error: " + www.error);
            callback(null);
        }

        Texture2D t = new Texture2D(2, 2);
        www.LoadImageIntoTexture(t);
        t.name = fileName;

        if (callback != null)
            callback(t);

        Destroy(gameObject);
    }
}
